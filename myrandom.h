#ifndef RANDOM_H
#define RANDOM_H

double myrandom() {
	// Return random in range [0, 1]
	return (double)rand()/RAND_MAX;
}

double myrandom(double max) {
	// Return random in range [0, max]
	return myrandom()*max;
}

double myrandom(double min, double max) {
	// Return random in range [min, max]
	return myrandom(max-min) + min;
}

#endif