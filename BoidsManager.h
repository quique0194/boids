#ifndef BOIDS_MANAGER_H
#define BOIDS_MANAGER_H

#include <vector>
using std::vector;

#include "./Boid.h"
#include "./Space.h"
#include "./myrandom.h"


class BoidsManager {
    public:
    	BoidsManager(int n, const Space& _space): boids(n, NULL), space(_space) {
            init_random();
    	}

        ~BoidsManager() {
            for (int i = 0; i < boids.size(); ++i) {
                delete boids[i];
            }
        }

        void init_random() {
            for (int i = 0; i < boids.size(); ++i) {
                boids[i] = new Boid();
                boids[i]->x = myrandom(space.left, space.right);
                boids[i]->y = myrandom(space.bottom, space.top);
                boids[i]->angle = myrandom(0, 360);
                boids[i]->speed = 0.5;
            }
        }

        void display() {
            for (int i = 0; i < boids.size(); ++i) {
                boids[i]->turn(myrandom(-1, 1));
                boids[i]->move();
                boids[i]->display();
            }
        }

	private:
		vector<Boid*> boids;
        Space space;
};

#endif