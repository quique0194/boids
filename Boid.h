#ifndef BOIDS_H
#define BOIDS_H

#include <cstdlib>
#include <cmath>

#include <iostream>
using namespace std;

class Boid {
	public:
		double x;	// center
		double y;	// center
		double angle;	// degrees: 0 look right, 180 look left, 90 look up
		double speed;	// amount of space moved between two displays

		void display() {
			glPushMatrix();
				glColor3f(0, 0, 255);
				glTranslatef(x, y, 0);
				glRotatef(angle, 0, 0, 1);
				glBegin(GL_POLYGON);
					glVertex2f(-5, -3);
					glVertex2f(-5, 3);
					glVertex2f(5, 0);
			    glEnd();
			glPopMatrix();
		}

		void move() {
			x += speed * cos(angle*M_PI/180);
			y += speed * sin(angle*M_PI/180);
		}

		void turn(double degrees) {
			angle += degrees;
		}
};

#endif