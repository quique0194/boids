#ifndef SPACE_H
#define SPACE_H

class Space {
	public:
		Space(double _left, double _right, double _bottom, double _top,
			  double _near_val, double _far_val):
				left(_left), right(_right), bottom(_bottom), top(_top),
				near_val(_near_val), far_val(_far_val) {
		}

		double left;
		double right;
		double bottom;
		double top;
		double near_val;
		double far_val;
};

#endif