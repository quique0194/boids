#include <cstdlib>
#include <ctime>
#include <cmath>

#include <GL/glut.h>

#include "./BoidsManager.h"
#include "./Space.h"


#define KEY_ESC 27
#define NUMBER_OF_BOIDS 50


Space space(-100, 100, -100, 100, -100, 100);
BoidsManager boids_manager(NUMBER_OF_BOIDS, space);


void displayGizmo()
{
    glBegin(GL_LINES);
    glColor3d(255,0,0);
    glVertex2d(0, 0);
    glVertex2d(1, 0);
    glColor3d(0, 255, 0);
    glVertex2d(0, 0);
    glVertex2d(0, 1);
    glEnd();
}


void window_display(void) {
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    displayGizmo();

    boids_manager.display();

    glutSwapBuffers();
}


void init_GL(void) {
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(space.left, space.right, space.bottom, space.top, space.near_val, 
    		space.far_val);
}


GLvoid window_key(unsigned char key, int x, int y) {
    switch (key) {
	    case KEY_ESC:
	        exit(0);
	        break;

	    default:
	        break;
    }
}


GLvoid window_idle() {
    glutPostRedisplay();
}


int main(int argc, char** argv) {
	srand(time(0));

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Boids");

    init_GL();

    glutDisplayFunc(window_display);
    glutKeyboardFunc(&window_key);
    glutIdleFunc(&window_idle);

    glutMainLoop();

    return 0;
}
